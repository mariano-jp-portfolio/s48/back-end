const express = require('express');
const router = express.Router();
const CarController = require('../controllers/carController');

router.post('/', (req, res) => {
	// request.body now contains base64 string
    CarController.convert(req.body).then(result => res.send(result));
    // after our controller converts it to a json array, it will go back here to be sent back as our backend api's response
})

module.exports = router;